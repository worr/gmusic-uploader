import mock
import re
import unittest

from ..config import Config, USERNAME, PASSWORD, MAX_CONCURRENT_UPLOADS, ConfigError, DIRECTORY, DB_FILE, INIT_SCAN, TRUTH_REGEXP, FALSE_REGEXP
from mock import patch, DEFAULT

class TestConfigurationFunctions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.patcher = mock.patch('ConfigParser.SafeConfigParser')
        cls.m = cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.patcher.stop()

    def setUp(self):
        def config_parser_side_effects(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == PASSWORD:
                return "butts"
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"
            elif args[1] == INIT_SCAN:
                return "True"

        self.m.return_value.read.return_value = True
        self.m.return_value.has_option.return_value = True
        self.m.return_value.get.side_effect = config_parser_side_effects

    @patch('os.stat')
    def test_valid_configuration(self, mock_stat):
        mock_stat.return_value.st_mode = 0o0700

        c = Config()

        self.assertEqual(c.username, "worr")
        self.assertEqual(c.password, "butts")
        self.assertEqual(c.concurrent_uploads, 2)
        self.assertEqual(c.db_file, "/var/db/gmusic-uploader.db")
        self.assertEqual(c.directory, "/home/will/music")
        self.assertTrue(c.init_scan)

    @patch('os.stat')
    def test_invalid_concurrent_uploads(self, mock_stat):
        def config_parser_side_effects_invalid_concurrent_uploads(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == PASSWORD:
                return "butts"
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return "foo"
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"
            elif args[1] == INIT_SCAN:
                return "True"

        self.m.return_value.get.side_effect = config_parser_side_effects_invalid_concurrent_uploads
        mock_stat.return_value.st_mode = 0

        self.assertRaisesRegexp(ValueError, r"^Max concurrent uploads is not an integer$", Config)

    @patch('os.stat')
    def test_no_username(self, mock_stat):
        def config_parser_side_effects_no_username(*args, **kwargs):
            if args[1] == PASSWORD:
                return "butts"
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"
            elif args[1] == INIT_SCAN:
                return "True"

        self.m.return_value.get.side_effect = config_parser_side_effects_no_username
        mock_stat.return_value.st_mode = 0
        self.assertRaisesRegexp(ValueError, r"^No username provided in config file$", Config)

    @patch('os.stat')
    def test_no_password(self, mock_stat):
        def config_parser_side_effects_no_password(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"
            elif args[1] == INIT_SCAN:
                return "True"

        self.m.return_value.get.side_effect = config_parser_side_effects_no_password
        mock_stat.return_value.st_mode = 0
        self.assertRaisesRegexp(ValueError, r"^No password provided in config file$", Config)

    @patch('os.stat')
    def test_no_directory(self, mock_stat):
        def config_parser_side_effects_no_directory(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == PASSWORD:
                return "butts"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"
            elif args[1] == INIT_SCAN:
                return "True"

        self.m.return_value.get.side_effect = config_parser_side_effects_no_directory
        mock_stat.return_value.st_mode = 0
        self.assertRaisesRegexp(ValueError, r"^No watch directory provided in config file$", Config)

    @patch('os.stat')
    def test_no_db_file(self, mock_stat):
        def config_parser_side_effects_no_db_file(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == PASSWORD:
                return "butts"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == INIT_SCAN:
                return "True"

        self.m.return_value.get.side_effect = config_parser_side_effects_no_db_file
        mock_stat.return_value.st_mode = 0
        self.assertRaisesRegexp(ValueError, r"^No db file provided in config file$", Config)

    @patch('os.stat')
    def test_no_init_scan(self, mock_stat):
        def config_parser_has_side_effects_no_init_scan(*args, **kwargs):
            if args[1] == INIT_SCAN:
                return False
            return DEFAULT

        def config_parser_side_effects_no_init_scan(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == PASSWORD:
                return "butts"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"

        self.m.return_value.get.side_effect = config_parser_side_effects_no_init_scan
        self.m.return_value.has_option.side_effect = config_parser_has_side_effects_no_init_scan
        mock_stat.return_value.st_mode = 0
        config = Config()
        self.assertFalse(config.init_scan)
        self.m.return_value.has_option.side_effect = None

    @patch('os.stat')
    def test_invalid_init_scan(self, mock_stat):
        def config_parser_side_effects_invalid_init_scan(*args, **kwargs):
            if args[1] == USERNAME:
                return "worr"
            elif args[1] == PASSWORD:
                return "butts"
            elif args[1] == MAX_CONCURRENT_UPLOADS:
                return 2
            elif args[1] == DIRECTORY:
                return "/home/will/music"
            elif args[1] == DB_FILE:
                return "/var/db/gmusic-uploader.db"
            elif args[1] == INIT_SCAN:
                return "asdf"

        self.m.return_value.get.side_effect = config_parser_side_effects_invalid_init_scan
        mock_stat.return_value.st_mode = 0
        self.assertRaisesRegexp(ValueError, r"^scan on start value invalid$", Config)

    @patch('os.stat')
    def test_invalid_configuration_permissions(self, mock_stat):
        mock_stat.return_value.st_mode = 0o0755
        self.assertRaisesRegexp(IOError, r"^Config file readable by people other than current user$", Config)

    def test_no_file(self):
        self.m.return_value.read.return_value = False
        self.assertRaises(IOError, Config)

    def test_truth_regexp(self):
        for test in [ "TRUE", "true", "YES", "yes", "tRuE", "yEs", "T", "t", "Y", "y" ]:
            self.assertTrue(TRUTH_REGEXP.match(test))
        
        for test in [ "tr", "", "tyes", "yy" ]:
            self.assertFalse(TRUTH_REGEXP.match(test))

    def test_false_regexp(self):
        for test in [ "FALSE", "false", "NO", "no", "FaLsE", "nO", "F", "f", "N", "n" ]:
            self.assertTrue(FALSE_REGEXP.match(test))

        for test in [ "fa", "", "fn", "ff" ]:
            self.assertFalse(FALSE_REGEXP.match(test))
