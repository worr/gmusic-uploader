import mock
import unittest

from ..uploader import Uploader, UploaderError, SUPPORTED_MIMETYPES
from ..config import Config

from mock import patch, PropertyMock

class TestUploaderClass(unittest.TestCase):

    @patch.object(Config, '__init__')
    @patch.object(Config, 'db_file', new_callable=PropertyMock)
    @patch('sqlite3.connect')
    @patch('mimetypes.guess_type')
    def test_check_file(self, mock_guess_type, mock_sqlite3_connect, mock_db_file, mock_config):
        mock_config.return_value = None
        mock_db_file.return_value = ''
        uploader = Uploader()

        for mimetype in SUPPORTED_MIMETYPES:
            mock_guess_type.return_value = ( mimetype, None )

            self.assertTrue(uploader.check_file('null'))

    @patch.object(Config, '__init__')
    @patch.object(Config, 'db_file', new_callable=PropertyMock)
    @patch('sqlite3.connect')
    @patch('mimetypes.guess_type')
    def test_check_invalid_file(self, mock_guess_type, mock_sqlite3_connect, mock_db_file, mock_config):
        mock_config.return_value = None
        mock_db_file.return_value = ''
        uploader = Uploader()

        mimetypes = ( 'application/exe', 'text/javascript', 'asdfasdf' )

        for mimetype in mimetypes:
            mock_guess_type.return_value = mimetype

            self.assertFalse(uploader.check_file('null'))

    @patch('sqlite3.connect')
    @patch('gmusicapi.Api.login')
    @patch('gmusicapi.Api.upload')
    @patch.object(Config, '__init__')
    @patch.object(Config, 'username', new_callable=PropertyMock)
    @patch.object(Config, 'password', new_callable=PropertyMock)
    @patch.object(Config, 'db_file', new_callable=PropertyMock)
    @patch.object(Uploader, 'check_file')
    def test_upload_file(self, mock_check_file, mock_db_file, mock_password, mock_username, mock_config, mock_api_upload, mock_api_login, mock_sqlite3_connect):
        mock_check_file.return_value = True
        mock_config.return_value = None
        mock_username.return_value = ''
        mock_password.return_value = ''
        mock_db_file.return_value = ''
        mock_api_login.return_value = True

        uploader = Uploader()

        mock_api_upload.return_value = { 'levels.mp3': 1 }

        self.assertTrue(uploader.upload_file('null'))
        mock_api_login.assert_called()
        mock_config.assert_called()
        mock_check_file.assert_called_with('null')

    @patch('sqlite3.connect')
    @patch('gmusicapi.Api.login')
    @patch('gmusicapi.Api.upload')
    @patch.object(Config, '__init__')
    @patch.object(Config, 'username', new_callable=PropertyMock)
    @patch.object(Config, 'password', new_callable=PropertyMock)
    @patch.object(Config, 'db_file', new_callable=PropertyMock)
    @patch.object(Uploader, 'check_file')
    def test_upload_file_fail(self, mock_check_file, mock_db_file, mock_password, mock_username, mock_config, mock_api_upload, mock_api_login, mock_sqlite3_connect):
        mock_api_upload.return_value = {}
        mock_check_file.return_value = True
        mock_config.return_value = None
        mock_username.return_value = ''
        mock_password.return_value = ''
        mock_db_file.return_value = ''
        mock_api_login.return_value = True

        uploader = Uploader()

        self.assertRaisesRegexp(UploaderError, r"failed to upload songs", uploader.upload_file, 'null')
        mock_api_login.assert_called()
        mock_check_file.assert_called_with('null')
        mock_config.assert_called()

    @patch('sqlite3.connect')
    @patch('gmusicapi.Api.login')
    @patch('gmusicapi.Api.upload')
    @patch.object(Config, '__init__')
    @patch.object(Config, 'username', new_callable=PropertyMock)
    @patch.object(Config, 'password', new_callable=PropertyMock)
    @patch.object(Config, 'db_file', new_callable=PropertyMock)
    @patch.object(Uploader, 'check_file')
    def test_upload_login_fail(self, mock_check_file, mock_db_file, mock_password, mock_username, mock_config, mock_api_upload, mock_api_login, mock_sqlite3_connect):
        mock_check_file.return_value = True
        mock_config.return_value = None
        mock_username.return_value = ''
        mock_password.return_value = ''
        mock_db_file.return_value = ''
        mock_api_login.return_value = False

        uploader = Uploader()

        self.assertRaisesRegexp(UploaderError, r"could not log in to Google music", uploader.upload_file, 'null')
        mock_api_login.assert_called()
        self.assertEqual(mock_check_file.call_count, 0)
