import ConfigParser
import os
import re
import stat

from os.path import expanduser

GOOGLE = "Google"
USERNAME = "username"
PASSWORD = "password"
MAX_CONCURRENT_UPLOADS = "max concurrent uploads"
DIRECTORY = "directory"
DB_FILE = "db file"

DAEMON = "Daemon"
INIT_SCAN = "scan on start"

TRUTH_REGEXP = re.compile(r"\b(?:t(?:rue)?|y(?:es)?)\b", re.I)
FALSE_REGEXP = re.compile(r"\b(?:f(?:alse)?|no?)\b", re.I)

class Config(object):
    def __init__(self):
        super(Config, self).__init__()

        self._init_scan = \
            self._db_file = \
            self._directory = \
            self._concurrent_uploads = ""
        self.read_configuration()

    @staticmethod
    def default_config_location():
        return expanduser("~/.gmusic-uploader.ini")

    def read_configuration(self):
        parser = ConfigParser.SafeConfigParser()
        if not parser.read(Config.default_config_location()):
            raise IOError("Could not read file")

        st = os.stat(Config.default_config_location())
        if not parser.has_option(GOOGLE, MAX_CONCURRENT_UPLOADS):
            raise ConfigError("Missing max concurrent uploads in config file")

        if not parser.has_option(GOOGLE, DIRECTORY):
            raise ConfigError("Missing watch directory in config file")

        if not parser.has_option(GOOGLE, DB_FILE):
            raise ConfigError("Missing db file in config file")

        if not parser.has_option(DAEMON, INIT_SCAN):
            self._init_scan = False

        if not self._directory:
            self._directory = parser.get(GOOGLE, DIRECTORY)
            if not self._directory:
                raise ValueError("No watch directory provided in config file")

        if not self._concurrent_uploads:
            try:
                self._concurrent_uploads = int(parser.get(GOOGLE, MAX_CONCURRENT_UPLOADS))
            except ValueError:
                raise ValueError("Max concurrent uploads is not an integer")

        if not self._db_file:
            self._db_file = parser.get(GOOGLE, DB_FILE)
            if not self._db_file:
                raise ValueError("No db file provided in config file")

        # Can be false, ergo check for ""
        if self._init_scan == "" and isinstance(self._init_scan, basestring):
            init_scan = parser.get(DAEMON, INIT_SCAN)
            if TRUTH_REGEXP.match(init_scan):
                self._init_scan = True
            elif FALSE_REGEXP.match(init_scan):
                self._init_scan = False
            else:
                raise ValueError("scan on start value invalid")

    @property
    def concurrent_uploads(self):
        if not self._concurrent_uploads:
            self.read_configuration()
        return self._concurrent_uploads

    @property
    def directory(self):
        if not self._directory:
            self.read_configuration()
        return self._directory

    @property
    def db_file(self):
        if not self._db_file:
            self.read_configuration()
        return self._db_file

    @property
    def init_scan(self):
        if not self._init_scan:
            self.read_configuration()
        return self._init_scan

class ConfigError(Exception):
    pass
