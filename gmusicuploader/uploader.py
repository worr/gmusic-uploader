from config import Config
from gmusicapi.clients import Musicmanager

import gmusicapi.clients
import time
import mimetypes
import os.path
import sqlite3

SUPPORTED_MIMETYPES = ( 'audio/mpeg', 'audio/mp4a-latm', 'audio/ogg', 'audio/flac', 'audio/x-ms-wma' )

class Uploader(object):
    def __init__(self):
        super(Uploader, self).__init__()
        self.config = Config()
        self.api = Musicmanager(validate=True, verify_ssl=True)
        self.progress = []

        if not os.path.isfile(gmusicapi.clients.OAUTH_FILEPATH):
            Musicmanager.perform_oauth()

        if not os.path.exists(self.config.db_file):
            db = sqlite3.connect(self.config.db_file)
            with db:
                db.execute("create table success ( filename varchar, date integer )")
                db.execute("create table failures ( filename varchar, date integer, message text )")
            db.close()

    def has_uploaded(self, filename):
        db = sqlite3.connect(self.config.db_file)
        ret = False
        with db:
            res = db.execute("select date from success where filename=? order by date desc", (filename,))
            ret = res.fetchone()
            
            if ret is not None:
                ret = ret[0]

        return ret

    def check_file(self, filename):
        mimetype = mimetypes.guess_type(filename)[0]
        return mimetype in SUPPORTED_MIMETYPES

    def upload_file(self, filename):
        self.config = Config()

        try:
            if not self.api.login():
                raise UploaderError("could not log in to Google music")
        except gmusicapi.exceptions.AlreadyLoggedIn:
            pass

        ret = {}
        db = sqlite3.connect(self.config.db_file)
        if self.check_file(filename):
            try:
                ret = self.api.upload(filename, enable_matching=True)
            except Exception as e:
                with db:
                    db.execute("insert into failures values (?, ?, ?)", (filename, time.time(), e.__str__()))
            else:
                with db:
                    db.execute("insert into success values (?, ?)", (filename, time.time()))
            finally:
                db.close()

            if not ret:
                raise UploaderError("failed to upload songs")
        else:
            raise UploaderError("{} is not a valid music file".format(filename))

        return True
        
class UploaderError(Exception):
    pass
