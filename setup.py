from distutils.core import setup
from setuptools import find_packages

setup(
    name='gmusic-uploader',
    version='0.1.0',
    author='William Orr',
    author_email='will@worrbase.com',
    packages=['gmusicuploader', 'gmusicuploader.test'],
    scripts=['scripts/gmusic-upload.py', 'scripts/gmusic-daemon.py'],
    url='http://worrbase.com/',
    license='LICENSE',
    description='Upload music to Google Music in the background',
    long_description=open('README.md').read(),
    test_suite='gmusicuploader.test',
    install_requires=[
        "watchdog >= 0.6.0",
        "gmusicapi >= 3.1.0",
        "mock >= 1.0.1"
    ]
)
