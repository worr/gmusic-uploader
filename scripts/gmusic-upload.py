#! python

from gmusicuploader.uploader import Uploader, UploaderError

import argparse

def main(files):
    uploader = Uploader()
    
    for f in files:
        if uploader.check_file(f):
            try:
                uploader.upload_file(f)
            except UploaderError as e:
                print e
                print "Cannot upload {}".format(f)
        else:
            print "Cannot upload {}".format(f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Upload songs to Google Music")
    parser.add_argument('files', metavar='file', type=str, nargs='+', help='list of files to upload')
    args = parser.parse_args()
    main(args.files)
