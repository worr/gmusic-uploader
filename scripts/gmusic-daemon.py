#! python

import os
import os.path
import time

from gmusicuploader.config import Config
from gmusicuploader.uploader import Uploader, UploaderError
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

class GmusicUploadHandler(FileSystemEventHandler):
    def __init__(self):
        super(GmusicUploadHandler, self).__init__()
        self.uploader = Uploader()

    def upload(self, filename):
        print filename
        try:
            self.uploader.upload_file(filename)
        except UploaderError as e:
            print e

    def on_created(self, event):
        if not event.is_directory:
            self.upload(event.src_path)

    def on_modified(self, event):
        if not event.is_directory:
            self.upload(event.src_path)

def main():
    config = Config()
    event_handler = GmusicUploadHandler()

    if config.init_scan:
        uploader = Uploader()
        for root, dirs, files in os.walk(config.directory):
            for f in files:
                fp = os.path.join(root, f)
                st = os.stat(fp)
                if uploader.check_file(fp) and not uploader.has_uploaded(fp) > st.st_mtime:
                    print fp
                    uploader.upload_file(fp)

    observer = Observer()
    observer.schedule(event_handler, path=config.directory, recursive=True)
    print "Watching directory {}".format(config.directory)
    observer.start()
    print "Starting watcher..."

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
        print "Exiting..."
    observer.join()

if __name__ == '__main__':
    main()
